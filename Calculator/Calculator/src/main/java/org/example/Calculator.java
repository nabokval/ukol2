package org.example;

public class Calculator {

    public int sum(int n1, int n2) {
        return n1 + n2;
    }

    public int substract(int n1, int n2) {
        return n1 - n2;
    }

    public int multiply(int n1, int n2) {
        return n1 * n2;
    }

    public int divide(int n1, int n2) throws Exception {
        if (n2 == 0) {
            throw new Exception("Divided by 0");
        }
        return n1 / n2;
    }
}
