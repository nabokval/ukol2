package org.example;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CalculatorTest {


    Calculator inst;

    @BeforeEach
    public void before() {
        inst = new Calculator();
    }

    @ParameterizedTest
    @CsvSource(value = {"1,2,3","2,3,5","3,5,8"})
    void myfirst(int a, int b, int exp){
        Calculator calculator = new Calculator();
        int sum = calculator.sum(a,b);
        assertEquals(exp, sum);
    }
    @Test
    @Order(0)
    void sum() {
        assertEquals(4, inst.sum(2, 2));
    }


    @Test
    @Order(1)
    void substract() {
        assertEquals(0, inst.substract(2, 2));
    }

    @Test
    @Order(3)
    void multiply() {
        assertEquals(6, inst.multiply(3, 2));
    }

    @Test
    @Order(4)
    void divide() throws Exception {
        assertEquals(1, inst.divide(2, 2));
    }



    @Test
    @Order(5)
    void divide_by_zero() {
        Assertions.assertThrows(Exception.class, () -> inst.divide(2, 0));
    }
}